package land.chipmunk.offlinemultiplayer;

import net.fabricmc.api.ModInitializer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OfflineMultiplayer implements ModInitializer {
  public static final Logger LOGGER = LogManager.getLogger("offline-multiplayer");

  @Override
  public void onInitialize() {
    LOGGER.info("Welcome to the rice fields!");
  }
}