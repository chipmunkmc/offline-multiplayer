package land.chipmunk.offlinemultiplayer.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(com.mojang.authlib.yggdrasil.YggdrasilSocialInteractionsService.class)
public class YggdrasilSocialInteractionsServiceMixin {
  @Shadow private boolean serversAllowed;
  @Shadow private boolean realmsAllowed;
  @Shadow private boolean chatAllowed;

  @Inject(at = @At("HEAD"), method = "checkPrivileges", cancellable = true, remap = false)
  private void checkPrivileges (CallbackInfo info) {
    serversAllowed = true;
    realmsAllowed = true;
    chatAllowed = true;
    info.cancel();
  }
}